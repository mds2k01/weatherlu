//
//  WeatherConn.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 11/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit

class WeatherConn: NSObject {

    let utils = Utils()
    let weather = Weather()
    
    func getCitiesAround(lat: String, lon: String, completion: @escaping ([[String: Any]], Bool)->()) {
    
        utils.checkConnection(flag: false, completionHandler: { (openWeather) in
            
            if logs == true {
                print("OpenWeather is reachable: \(openWeather)")
            }
            
            if (openWeather == false) {
                
                if logs == true {
                    print("Error: cannot access OpenWeather")
                }
                
                completion([[:]], false)
                return
            }
            
        })
        
        guard let url = URL(string: utils.urlAroundFromOpenWeather(lat: lat, lon: lon)) else {
            
            if logs == true {
                print("Error: cannot create URL")
            }
            
            completion([[:]], true)
            return
        }
        
        if logs == true {
            print("URL OpenWeather: \(utils.urlAroundFromOpenWeather(lat: lat, lon: lon))")
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeOutInterval)
        urlRequest.httpMethod = "GET"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if logs == true {
                    print(httpResponse.statusCode)
                }
                
            }
            
            if (error?.isConnectivityError == true) {
                completion([[:]], true)
                return
            }
            
            guard error == nil else {
                
                if logs == true {
                    print(error!)
                }
                completion([[:]], true)
                return
            }
            
            guard let responseData = data else {
                
                if logs == true {
                    print("Error: did not receive data")
                }
                completion([[:]], true)
                return
            }
            
            self.weather.parseMyData(data: responseData, lat: lat, lon: lon, completion: { (arrCities) in
                
                completion(arrCities, true)
            })
            
        }
        task.resume()
    }
    
}


extension Error {
    
    var isConnectivityError: Bool {
        // let code = self._code || Can safely bridged to NSError, avoid using _ members
        let code = (self as NSError).code
        
        if (code == NSURLErrorTimedOut) {
            if logs == true {
                print("Error: Timeout")
            }
            return true // time-out
        }
        
        if (self._domain != NSURLErrorDomain) {
            return false // Cannot be a NSURLConnection error
        }
        
        switch (code) {
        case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost, NSURLErrorCannotConnectToHost:
            return true
        default:
            return false
        }
    }
    
}
