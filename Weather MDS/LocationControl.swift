//
//  LocationControl.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 13/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit
import MapKit

protocol LocationProtocol {

    func updateData(arrRaw:[[String: Any]], connOpenWeather: Bool)
    func openSettingsLocation()
    func blockedConnection()
}

//extension LocationProtocol {
//    
//    func openSettingsLocation() {}
//}


class LocationControl: NSObject {

    let locationManager = CLLocationManager()
    let weatherApi = WeatherConn()
    var delegate: LocationProtocol?
    var citiesArray = [[String: Any]]()
    
    func startLocation() {
        
        //Reset time
        UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
        
        //Location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func stopLocation() {
        
        locationManager.stopUpdatingLocation()
    }
    
    func restartLocation() {
        
        locationManager.startUpdatingLocation()
    }
    
    
}

extension LocationControl: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .notDetermined {
            
            return
        }
        else if status == .authorizedWhenInUse {
            
            UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
            locationManager.startUpdatingLocation()
        }
        else {
            
            if logs == true {
                print("Not allowed")
            }
            
            self.delegate?.openSettingsLocation()
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
 
        let latitude: CLLocationDegrees = (locationManager.location?.coordinate.latitude)!
        let longitude: CLLocationDegrees = (locationManager.location?.coordinate.longitude)!
        //        let location = CLLocation(latitude: latitude, longitude: longitude)
        if logs == true {
            print("lat: \(latitude) | lon: \(longitude)")
        }
        
        //Recover last date
        let defaults = UserDefaults.standard
        let lastDate: Date = defaults.date(forKey: "LastDate") ?? Date()
        
        if logs == true {
            print("Interval between connections: \(intervalBetweenConnections)")
        }
        
        if (lastDate.seconds(from: Date()) >= intervalBetweenConnections) {
            
            self.getListCitiesAround(latitude: latitude, longitude: longitude)
            defaults.set(date: Date(), forKey: "LastDate")
        }
//        else {
//
//            if logs == true {
//                print("Blocked connection")
//            }
//
//            self.delegate?.blockedConnection()
//        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if logs == true {
            print("error: \(error)")
        }
    }
    
    
    
    func getListCitiesAround(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        weatherApi.getCitiesAround(lat: "\(latitude)", lon: "\(longitude)") { (citiesWeather, connOpenWeather) in
            
            if logs == true {
                print("Array of Cities: \(citiesWeather)")
            }
            
            self.citiesArray = citiesWeather
            self.delegate?.updateData(arrRaw: self.citiesArray, connOpenWeather: connOpenWeather)
//            NotificationCenter.default.post(name: .didCompleteUpdate, object: nil)
        }
    }
}




//extension UserDefaults {
//    
//    func set(location:CLLocation, forKey key: String) {
//        let locationLat = NSNumber(value:location.coordinate.latitude)
//        let locationLon = NSNumber(value:location.coordinate.longitude)
//        self.set(["lat": locationLat, "lon": locationLon], forKey:key)
//    }
//    
//    func location(forKey key: String) -> CLLocation? {
//        if let locationDictionary = self.object(forKey: key) as? Dictionary<String,NSNumber> {
//            let locationLat = locationDictionary["lat"]!.doubleValue
//            let locationLon = locationDictionary["lon"]!.doubleValue
//            return CLLocation(latitude: locationLat, longitude: locationLon)
//        }
//        return nil
//    }
//    
//    func set(date:Date, forKey key: String) {
//        self.set(date, forKey: key)
//    }
//    
//    func date(forKey key: String) -> Date? {
//        return self.object(forKey: key) as? Date
//    }
//}
