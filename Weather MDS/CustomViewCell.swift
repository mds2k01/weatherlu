//
//  CustomViewCell.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 13/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit

class CustomViewCell: UITableViewCell {

    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblCityTemp: UILabel!
    @IBOutlet weak var lblCityDistance: UILabel!
    @IBOutlet weak var imgCityWeather: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
