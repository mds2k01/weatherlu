//
//  ListWeatherViewController.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 13/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit
//import MapKit

class ListWeatherViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, LocationProtocol, MapViewProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblMainName: UILabel!
    @IBOutlet weak var lblMainTemp: UILabel!
    @IBOutlet weak var lblMin: UILabel!
    @IBOutlet weak var lblMax: UILabel!
    @IBOutlet weak var lblMainTempMin: UILabel!
    @IBOutlet weak var lblMainTempMax: UILabel!
    @IBOutlet weak var btChangeUnit: UIButton!
    @IBOutlet weak var btMap: UIButton!
    @IBOutlet weak var btHere: UIButton!
    
    @IBOutlet weak var imgMainWeather: UIImageView!
    @IBOutlet weak var imgConnection: UIImageView!
    
    var arrCitiesRaw = [[String: Any]]()
    var arrCities = [[String: Any]]()
    
    var image: UIImage!
    
    let locationControl = LocationControl()
    
    let notificationCenter = NotificationCenter.default
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        locationControl.delegate = self
//        locationControl.startLocation()
        
        DispatchQueue.main.async {
            
            self.locationControl.delegate = self
            self.locationControl.startLocation()
            
            //Reset time
            UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
            
            Utils.shared.showLoading(vc: self)
        }

        // Do any additional setup after loading the view.
        
        initialSetupLabels()
        
        hideAll()
        imgConnection.isHidden = true
        
        notificationCenter.addObserver(self, selector: #selector(locateHere(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
//        //Reset time
//        UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
        
        //Units Default
        if (UserDefaults.standard.string(forKey: "unitTemp") == nil) {
            UserDefaults.standard.set("c", forKey: "unitTemp")
            btChangeUnit.setTitle("°C", for: .normal)
        }
        else if UserDefaults.standard.string(forKey: "unitTemp") == "c" {
            btChangeUnit.setTitle("°C", for: .normal)
        } else {
            btChangeUnit.setTitle("°F", for: .normal)
        }
        
        tableView.dataSource = self
        tableView.delegate = self

        let nib = UINib.init(nibName: "CustomViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomViewCell")
    
//        Utils.shared.showLoading(vc: self)
    }
    
    func initialSetupLabels() {
        
        imgConnection.isHidden = true
        lblMainName.text = "--"
        lblMainTemp.text = "--"
        lblMainTempMin.text = "--"
        lblMainTempMax.text = "--"
        
    }
    
    
    @IBAction func changeTempUnit(_ sender: UIButton) {
        
        initialSetupLabels()
        
        if UserDefaults.standard.string(forKey: "unitTemp") == "f" {
            UserDefaults.standard.set("c", forKey: "unitTemp")
            btChangeUnit.setTitle("°C", for: .normal)
        } else {
            UserDefaults.standard.set("f", forKey: "unitTemp")
            btChangeUnit.setTitle("°F", for: .normal)
        }
        
        fillMainInfo()
        tableView.reloadData()
    }
    
    
    @IBAction func openMap(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            Utils.shared.hideLoading(vc: self)
        }
        
        let mapView = MapViewController(nibName: "MapViewController", bundle: nil)
        mapView.delegate = self
        mapView.arrCitiesRaw = arrCitiesRaw
        mapView.fromList = true
        self.addChild(mapView)
        self.view .addSubview(mapView.view)
        mapView.didMove(toParent: self)
        
    }
    
    
    @IBAction func locateHere(_ sender: UIButton) {
     
        Utils.shared.showLoading(vc: self)
        
        imgConnection.isHidden = true
        
        //Reset time
        UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
        
        if (self.tableView.numberOfRows(inSection: 0) > 0) {
            
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
        
        self.imgConnection.isHidden = true
        
        self.locationControl.stopLocation()
        self.locationControl.restartLocation()
        
    }
    
    
    func hideAll() {
        
        DispatchQueue.main.async {
            
            self.tableView.isHidden = true
            self.lblMainName.isHidden = true
            self.lblMainTemp.isHidden = true
            self.lblMainTempMin.isHidden = true
            self.lblMainTempMax.isHidden = true
            self.lblMin.isHidden = true
            self.lblMax.isHidden = true
            self.btChangeUnit.isHidden = true
            self.imgMainWeather.isHidden = true
            
        }
    }
    
    
    func showAll() {
    
        DispatchQueue.main.async {
            
            self.tableView.isHidden = false
            self.lblMainName.isHidden = false
            self.lblMainTemp.isHidden = false
            self.lblMainTempMin.isHidden = false
            self.lblMainTempMax.isHidden = false
            self.lblMin.isHidden = false
            self.lblMax.isHidden = false
            self.btChangeUnit.isHidden = false
            self.imgMainWeather.isHidden = false
            self.imgConnection.isHidden = true
        }
        
    }
    
    
    func updateData(arrRaw: [[String : Any]], connOpenWeather: Bool) {
        
        if logs == true {
            print("Received array: \(arrRaw)")
        }
        
        if (connOpenWeather == false) {
            
            DispatchQueue.main.async {
                
                Utils.shared.hideLoading(vc: self)
                self.hideAll()
                self.imgConnection.isHidden = false
            }
            
        }
        else {
            
            DispatchQueue.main.async {
                
                self.showAll()
                
                self.arrCitiesRaw = arrRaw
                self.fillMainInfo()
                self.removeMainCityFromList()
                self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
                self.tableView.reloadData()
                
                Utils.shared.hideLoading(vc: self)
            }
        }
        
    }
    
    
    func updateDataFromMap(arrRaw: [[String : Any]]) {
        
        if (tableView.numberOfRows(inSection: 0) > 0) {
            
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
        
        if (arrRaw.count > 1) {
            
            locationControl.stopLocation()
            
            DispatchQueue.main.async {
                self.arrCitiesRaw = arrRaw
                self.fillMainInfo()
                self.removeMainCityFromList()
                self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
                self.tableView.reloadData()
                
                Utils.shared.hideLoading(vc: self)
            }
        }
        else {
            
            locationControl.stopLocation()
            locationControl.restartLocation()
            
            Utils.shared.hideLoading(vc: self)
        }
        
    }
    
    
    func blockedConnection() {
        
        DispatchQueue.main.async {
            
            Utils.shared.hideLoading(vc: self)
        }
    }
    
    
    
    func openSettingsLocation() {
        
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        
        let clAction = UIAlertAction(title: "Cancel", style: .default, handler: {(clAction) in
            //Show alert again
            self.openSettingsLocation()
        })
        
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        
        alertController.addAction(clAction)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCities.count - 1
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomViewCell", for: indexPath) as! CustomViewCell
        
        let dict = arrCities[indexPath.row]
        
        let temp: Double = dict["temp"] as! Double
        let tempC = temp.convertKelvinToCelsius()
        let tempF = temp.convertKelvinToFahrenheit()
        
        let distance: Double = dict["distance"] as! Double
        let distanceInKm = distance.convertMetersToKilometers()
        let distanceFormatted: String = NSString(format: "%.2f km", distanceInKm) as String
        
        let urlIcon: String = dict["icon"] as! String
        cell.imgCityWeather.setImageFromUrl(ImageURL: urlIcon)
        
        cell.lblCityName.text = "\(dict["name"] ?? "Ops")"
        
        if UserDefaults.standard.string(forKey: "unitTemp") == "f" {
            cell.lblCityTemp.text = "\(tempF)°"
        } else {
            cell.lblCityTemp.text = "\(tempC)°"
        }
        
        cell.lblCityDistance.text = "\(distanceFormatted)"
        
        return cell
        
    }

    
    func fillMainInfo() {
        
        if (arrCitiesRaw.count > 1) {
            
            let dict = arrCitiesRaw[0]
            
            let temp: Double = dict["temp"] as! Double
            let tempC = temp.convertKelvinToCelsius()
            let tempF = temp.convertKelvinToFahrenheit()
            
            let tempMin: Double = dict["tempMin"] as! Double
            let tempMinC = tempMin.convertKelvinToCelsius()
            let tempMinF = tempMin.convertKelvinToFahrenheit()
            
            let tempMax: Double = dict["tempMax"] as! Double
            let tempMaxC = tempMax.convertKelvinToCelsius()
            let tempMaxF = tempMax.convertKelvinToFahrenheit()
            
            let urlIcon: String = dict["icon"] as! String
            
            lblMainName.text = "\(dict["name"] ?? "Ops")"
            imgMainWeather.setImageFromUrl(ImageURL: urlIcon)
            
            if UserDefaults.standard.string(forKey: "unitTemp") == "c" {
                lblMainTemp.text = "\(tempC)°"
                lblMainTempMin.text = "\(tempMinC)°"
                lblMainTempMax.text = "\(tempMaxC)°"
            } else {
                lblMainTemp.text = "\(tempF)°"
                lblMainTempMin.text = "\(tempMinF)°"
                lblMainTempMax.text = "\(tempMaxF)°"
            }
            
            
        }
        
    }
    
    func removeMainCityFromList() {
        
        arrCities = arrCitiesRaw
        
        if (arrCities.count > 0) {
            arrCities.remove(at: 0)
        }
        
    }
}


extension UIImageView {
    
    func setImageFromUrl(ImageURL :String) {
        URLSession.shared.dataTask( with: NSURL(string:ImageURL)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                if let data = data {
                    self.image = UIImage(data: data)
                }
            }
        }).resume()
    }
}
