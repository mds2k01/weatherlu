//
//  Weather.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 13/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit

class Weather: NSObject {
    
    

    func parseMyData(data: Data, lat: String, lon: String, completion: @escaping ([[String: Any]])->()) {
        
        let utils = Utils()
        var citiesWeather = [[String: Any]]()
        var cities = [String]()
        
        //Parse
        guard let content = try? JSONDecoder().decode(WeatherNow.self, from: data) else {
            completion([[:]])
            return
        }
        
        let arrCities: Array = content.list
        
//        var countAdded: Int = 0
        var countRemoved: Int = 0
        
        if (arrCities.count > 0) {
            
            for i in 0...arrCities.count - 1 {
                
                let cityData = arrCities[i]
                let cityId = cityData.id
                let cityName = cityData.name
                let cityLat = cityData.coord.lat
                let cityLon = cityData.coord.lon
                let cityTemp = cityData.main.temp
                let cityTempMax = cityData.main.temp_max
                let cityTempMin = cityData.main.temp_min
                
                //            let cityTempC:Int = Int(cityTemp.convertKelvinToCelsius())
                //            let cityTempF:Int = Int(cityTemp.convertKelvinToFahrenheit())
                //
                //            let cityTempMaxC:Int = Int(cityTempMax.convertKelvinToCelsius())
                //            let cityTempMaxF:Int = Int(cityTempMax.convertKelvinToFahrenheit())
                //
                //            let cityTempMinC:Int = Int(cityTempMin.convertKelvinToCelsius())
                //            let cityTempMinF:Int = Int(cityTempMin.convertKelvinToFahrenheit())
                //
                let weatherDesc = cityData.weather[0].main!
                let weatherIcon = cityData.weather[0].icon!.urlForIcon()
                
                let distance = utils.calculateDistanceFromMe(myLat: lat, myLon: lon, toLat: "\(cityLat)", toLon: "\(cityLon)")
                
                if (Int(distance) <= metersAllowedToShow) {
                    
                    //                countAdded += 1
                    //                print("Included:")
                    //                print("[\(countAdded)] - \(cityName) (\(cityId)) - Lat:\(cityLat)|Lon:\(cityLon)|Distance: \(distance )")
                    //                print("Temp: \(cityTempC)*\(cityTempF) | Max: \(cityTempMaxC)*\(cityTempMaxF) Min: \(cityTempMinC)*\(cityTempMinF)")
                    //                print("Weather: \(weatherDesc) | Icon: \(weatherIcon)")
                    
                    
                    let contains = cities.contains(where: { $0 == cityName })
                    
                    if (contains == false) {
                        
                        let dict = ["name": cityName, "id": cityId as NSNumber, "lat": cityLat as NSNumber, "lon": cityLon as NSNumber, "distance": distance as NSNumber, "temp": cityTemp as NSNumber, "tempMax": cityTempMax as NSNumber, "tempMin": cityTempMin as NSNumber, "icon": weatherIcon, "description": weatherDesc] as Dictionary
                        
                        cities.append(cityName)
                        citiesWeather.append(dict)
                    }
                    else {
                        
                        if logs == true {
                            print("Duplicated: \(cityName)")
                        }
                    }
                    
                }
                else {
                    
                    if logs == true {
                        countRemoved += 1
                        print("Removed:")
                        print("[\(countRemoved)] - \(cityName) (\(cityId)) - Lat:\(cityLat)|Lon:\(cityLon)|Distance: \(distance)")
                        //                print("Temp: \(cityTempC)*\(cityTempF) | Max: \(cityTempMaxC)*\(cityTempMaxF) Min: \(cityTempMinC)*\(cityTempMinF)")
                        //                print("Weather: \(weatherDesc) | Icon: \(weatherIcon)")
                    }
                }
            }
            
//        print("Array of Cities: \(citiesWeather)")
            let citiesOrdered = orderCitiesByDistance(arrToOrder: citiesWeather)
            completion(citiesOrdered)
        }
        else {
            
            if logs == true {
                print("No results")
            }
            completion([[:]])
        }
        
        

    }
    
    
    func orderCitiesByDistance(arrToOrder: [[String: Any]])->[[String: Any]] {
        
        let sortedCitiesByDistance = (arrToOrder as NSArray).sortedArray(using: [NSSortDescriptor(key: "distance", ascending: true)]) as! [[String:AnyObject]]
        
        return sortedCitiesByDistance
    }

}

struct WeatherNow: Codable {
    
    let list: [WeatherDetail]
    
    enum CodingKeys: String, CodingKey {
        
        case list = "list"
    }
}

struct WeatherDetail: Codable {
    
    let name: String
    let id: Int
    let coord: CityCoordinate
    let main: CityTemp
    let weather: [CityWeather]
}

struct CityCoordinate: Codable {
    
    let lat: Float
    let lon: Float
}

struct CityTemp: Codable {
    
    let temp: Double
    let humidity: Int
    let temp_min: Double
    let temp_max: Double
}

struct CityWeather: Codable {
    
    let icon: String?
    let main: String?
}
