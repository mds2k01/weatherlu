//
//  Utils.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 11/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit
import MapKit

public let logs:Bool = false
let baseUrlOpenWeather: String = "https://api.openweathermap.org/data/2.5/"
let appID: String = "6e1d0e637df54d81423f84302a6512c9"
let maxResults: String = "50"
let baseUrlImgIcon: String = "https://openweathermap.org/img/w/"
public let intervalBetweenConnections: Int = 5 //seconds
public let metersAllowedToShow: Int = 50000 //meters
public let timeOutInterval: TimeInterval = 20.0 //seconds

class Utils: NSObject {
    
    func urlAroundFromOpenWeather(lat: String, lon: String)-> String {

        return baseUrlOpenWeather + "find?lat=" + lat + "&lon=" + lon + "&cnt=" + maxResults + "&APPID=" + appID
    }
    
    
    func calculateDistanceFromMe(myLat: String, myLon: String, toLat: String, toLon: String)->Double {
        
        let myLocation = CLLocation(latitude: (myLat as NSString).doubleValue, longitude: (myLon as NSString).doubleValue)
        let toLocation = CLLocation(latitude: (toLat as NSString).doubleValue as CLLocationDegrees, longitude: (toLon as NSString).doubleValue as CLLocationDegrees)
        
        let distance: CLLocationDistance = myLocation.distance(from: toLocation)
        
        return distance
    }
    
    
    func checkConnection(flag:Bool, completionHandler:@escaping (Bool) -> ())
    {
        
        DispatchQueue.main.async {
            
            let url = URL(string: "https://openweathermap.org")
            let request = NSMutableURLRequest(url: url!)
            
            request.httpMethod = "HEAD"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.timeoutInterval = 10.0
            
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) -> Void in
                
                let rsp = response as! HTTPURLResponse?
                completionHandler(rsp?.statusCode == 200)
                
            })
            task.resume()
        }
        
    }
    
    
    
    static let shared = Utils()
    
    let loadView = LoadViewController(nibName: "LoadViewController", bundle: nil)
    
    func showLoading(vc: UIViewController) {
        
        loadView.stopAnimateSyncIcon()
        loadView.view.removeFromSuperview()
        
        loadView.startAnimateSyncIcon()
        
        loadView.view.frame.origin.y = 0
        loadView.view.frame.size = UIScreen.main.bounds.size
        
        vc.addChild(loadView)
        vc.view .addSubview(loadView.view)
        loadView.didMove(toParent: vc)
    }
    
    func hideLoading(vc: UIViewController) {
        
        loadView.stopAnimateSyncIcon()
        loadView.view.removeFromSuperview()
    }
    
}


public extension Double {
    
    func convertKelvinToFahrenheit() -> Int {
        let recipe: Double = 1.8 * (self - 273) + 32
        return Int(recipe)
    }
    
    func convertKelvinToCelsius() -> Int {
        let recipe: Double = self - 273.15
        return Int(recipe)
    }
}

public extension String {
    
    func urlForIcon() -> String {
        
        return baseUrlImgIcon + self + ".png"
    }
}


public extension Double {
    
    func convertMetersToKilometers() -> Float {
        
        let converted = Float(self / 1000)
        return converted
    }
}

public extension Date {
    
    func minutes(from date: Date) -> Int {
        
        if logs == true {
            print("Minutes: \(abs(Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0))")
        }
        return abs(Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0)
    }
    
    func seconds(from date: Date) -> Int {
        
        if logs == true {
            print("Seconds: \(abs(Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0))")
        }
        return abs(Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0)
    }
}

public extension Notification.Name {
    
    static let didCompleteUpdate = Notification.Name("didCompleteUpdate")
}

public extension UserDefaults {
    
    func set(date:Date, forKey key: String) {
        self.set(date, forKey: key)
    }
    
    func date(forKey key: String) -> Date? {
        return self.object(forKey: key) as? Date
    }
}



@IBDesignable class GradientView: UIView {
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    @IBInspectable
    var startColor: UIColor = UIColor(red: 175/255.0, green: 194/255.0, blue: 184/255.0, alpha: 1.0) {
        
        didSet {
            configure()
        }
    }
    @IBInspectable
    var middleColor: UIColor = UIColor(red: 43/255.0, green: 121/255.0, blue: 140/255.0, alpha: 1.0) {
        
        didSet {
            configure()
        }
    }
    @IBInspectable
    var endColor: UIColor = UIColor(red: 20/255.0, green: 76/255.0, blue: 99/255.0, alpha: 1.0) {
        
        didSet {
            configure()
        }
    }
    
    func configure() {
        
        let colors:Array = [startColor.cgColor, middleColor.cgColor, endColor.cgColor]
        
        let layer = self.layer as! CAGradientLayer
        layer.colors = colors
        layer.startPoint = CGPoint(x: 0.0, y: 0.0)
        layer.endPoint = CGPoint(x: 0.0, y: 0.9)
    }
}


@IBDesignable
class RoundView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
}


@IBDesignable
class RoundButtonView: UIButton {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowRadius = 3.0
            layer.shadowOpacity = 0.3
            layer.shadowOffset = CGSize(width: 4, height: 4)
            layer.masksToBounds = false
        }
    }
}

