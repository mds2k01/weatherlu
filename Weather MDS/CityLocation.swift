//
//  CityLocation.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 14/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import Foundation
import MapKit

class CityLocation: NSObject, MKAnnotation {

    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}
