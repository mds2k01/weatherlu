//
//  MapViewController.swift
//  Weather MDS
//
//  Created by Marcelo Santos on 14/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewProtocol {
    
    func updateDataFromMap(arrRaw:[[String: Any]])
}


class MapViewController: UIViewController, LocationProtocol, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imgConnection: UIImageView!
    
    let locationControl = LocationControl()
    var arrCitiesRaw = [[String: Any]]()
    let weatherApi = WeatherConn()
    
    var delegate: MapViewProtocol?
    
    var lockMap: Bool?
    var applyZoom: Bool?
    var fromList: Bool?
    
    let notificationCenter = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        notificationCenter.addObserver(self, selector: #selector(close(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        locationControl.delegate = self
        locationControl.startLocation()
        
        lockMap = true
        applyZoom = true
        
        let mapDragRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(gestureRecognizer:)))
        mapDragRecognizer.delegate = self
        self.mapView.addGestureRecognizer(mapDragRecognizer)
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func didDragMap(gestureRecognizer: UIGestureRecognizer) {
        if (gestureRecognizer.state == UIGestureRecognizer.State.began) {
            if logs == true {
                print("Map drag began")
            }
            
            for annotation in self.mapView.annotations {
                self.mapView.removeAnnotation(annotation)
            }

        }
        
        if (gestureRecognizer.state == UIGestureRecognizer.State.ended) {
            
            if logs == true {
                print("Map drag ended")
            }
            
            //Reset time
            UserDefaults.standard.set(date: Date().addingTimeInterval(TimeInterval(intervalBetweenConnections + 1)), forKey: "LastDate")
            lockMap = false
            fromList = false
        }
    }
    
    
    
    func updateData(arrRaw: [[String : Any]], connOpenWeather: Bool) {
        
        if logs == true {
            print("Received array Map: \(arrRaw)")
        }
        
        if (fromList == false) {
            
            arrCitiesRaw = arrRaw
        }
        
        
        if (connOpenWeather == false) {
            
            DispatchQueue.main.async {
            
                self.mapView.isHidden = true
                self.imgConnection.isHidden = false
            }
        }
        else {
            
            DispatchQueue.main.async {
                
                self.mapView.isHidden = false
                self.imgConnection.isHidden = true
                
                if (self.arrCitiesRaw.count > 1) {
                    
                    for i in 0...self.arrCitiesRaw.count - 1 {
                        
                        let dict = self.arrCitiesRaw[i]
                        let cityName: String = dict["name"] as! String
                        let latitude: CLLocationDegrees = dict["lat"] as! CLLocationDegrees
                        let longitude: CLLocationDegrees = dict["lon"] as! CLLocationDegrees
                        
                        let temp: Double = dict["temp"] as! Double
                        let tempC = temp.convertKelvinToCelsius()
                        let tempF = temp.convertKelvinToFahrenheit()
                        
                        var temperature = "\(tempC)°"
                        if UserDefaults.standard.string(forKey: "unitTemp") == "f" {
                            temperature = "\(tempF)°"
                        }
                        
                        let city = CityLocation(title: cityName, subtitle: temperature, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                        
                        self.mapView.addAnnotation(city)
                    }
                } else {
                    
                    if logs == true {
                        print("No results")
                    }
                    
                }
                
                
                //Zoom interest area
                
                if (self.applyZoom == true) {
                    
                    //                self.mapView.fitAll()
                    self.mapView.showAnnotations(self.mapView.annotations, animated: true)
                }
                
            }
        }
        
    }
    
    func blockedConnection() {
        
    }
    
    
    func openSettingsLocation() {
        
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        
        let clAction = UIAlertAction(title: "Cancel", style: .default, handler: {(clAction) in
            //Show alert again
            self.openSettingsLocation()
        })
        
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        
        alertController.addAction(clAction)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "City"
        
        if annotation is CityLocation {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            }
            else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true

                let btn = UIButton(type: .detailDisclosure)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        if (lockMap == false) {
        
            let center = mapView.centerCoordinate
            
            let mapLatitude = center.latitude
            let mapLongitude = center.longitude
            
            if logs == true {
                print("new latitude: \(mapLatitude) | new longitude: \(mapLongitude)")
            }
            
            weatherApi.getCitiesAround(lat: "\(mapLatitude)", lon: "\(mapLongitude)") { (arrRaw, connOpenWeather) in
                
                if (connOpenWeather == false) {
                    
                    DispatchQueue.main.async {
                        
                        self.mapView.isHidden = true
                        self.imgConnection.isHighlighted = false
                    }
                }
                else {
                    
                    self.applyZoom = false
                    self.updateData(arrRaw: arrRaw, connOpenWeather: true)
                }
                
            }
            
        }
    
    }
    
    
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        lockMap = false
//
//    }
    
    
    
    @IBAction func close(_ sender: UIButton) {
        
        notificationCenter.removeObserver(self)
        self.delegate?.updateDataFromMap(arrRaw: arrCitiesRaw)
        self.view .removeFromSuperview()
    }
    

}


extension MKMapView {
    
    /// When we call this function, we have already added the annotations to the map, and just want all of them to be displayed.
    func fitAll() {
        var zoomRect            = MKMapRect.null;
        for annotation in annotations {
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
            zoomRect            = zoomRect.union(pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
    /// We call this function and give it the annotations we want added to the map. we display the annotations if necessary
    func fitAll(in annotations: [MKAnnotation], andShow show: Bool) {
        var zoomRect:MKMapRect  = MKMapRect.null
        
        for annotation in annotations {
            let aPoint          = MKMapPoint(annotation.coordinate)
            let rect            = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)
            
            if zoomRect.isNull {
                zoomRect = rect
            } else {
                zoomRect = zoomRect.union(rect)
            }
        }
        if(show) {
            addAnnotations(annotations)
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
    
}
