//
//  LoadViewController.swift
//  Meu Dia
//
//  Created by Marcelo Santos on 02/04/19.
//  Copyright © 2019 Marcelo dos Santos. All rights reserved.
//

import UIKit

class LoadViewController: UIViewController {

    @IBOutlet weak var imgSync: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    
    
    func startAnimateSyncIcon() {
        
        DispatchQueue.main.async {
            
            let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
            rotationAnimation.duration = 2.5 //Velocity
            rotationAnimation.isCumulative = true
            rotationAnimation.repeatCount = .infinity
            
            self.imgSync?.layer.add(rotationAnimation, forKey: "rotationAnimation")
        }
    }
    
    func stopAnimateSyncIcon() {
        
        DispatchQueue.main.async {
            
            self.imgSync?.layer.removeAnimation(forKey: "rotationAnimation")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
